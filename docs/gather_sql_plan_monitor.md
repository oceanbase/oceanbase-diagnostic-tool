## gather plan_monitor命令
```shell script
./obdiag gather plan_monitor [-h] [--from datetime datetime] [--to datetime datetime] [--since 'n'<m|h|d>] [--store_dir store_dir] --trace_id
                                     trace_id

Example: ./obdiag gather plan_monitor --trace_id xxxxx
```

